<h2>کپی تریدینگ</h2>

شما به عنوان یک تازه‌کار که زمان کافی برای آموزش دیدن به صورت حرفه‌ای ندارید، می‌توانید پیرو افراد حرفه‌ای باشید و به عبارت ساده‌تر تا زمانی که ساز و کار بازار را یاد نگرفتید، جا پای آن‌ها بگذارید. به این پیروی کپی تریدینگ (Copy Trading) می‌گویند. خب این معنا که شما یک کارگزار را که از کپی تریدینگ پشتیبانی می‌کند، انتخاب می‌کنید. در بین چندین فرد با تجربه به سراغ بهترین گزینه بروید و تمامی قدم‌هایی را که در بازار مالی ارز دیجیتال برمی‌دارد را تقلید می‌کنید.


<h2>ساختار کلی</h2>
در ساختار کپی تردینگ دو نقش کپی شونده و کپی کننده  تعریف میشود به این صورت که تمام معاملات و سفارشاتی که برای شخص کپی شونده باز میشود برای کپی کننده نیز عینا به صورت اتومات انجام شود.
به ازای هر کپی شونده و کپی کننده یک سوکت با صرافی مورد نظر باز میشود.
این برنامه فقط صرافی های زیر را پشتیبانی میکند:

1. Binance Spot
2. ‌Bitmex


<h3>Actions</h3>
در این فایل کلاس های زیر تعریف شده است که هر یک برای یکی از عملیات باز کردن سفارش جدید٬ کنسل کردن و بستن سفارش تعریف شده است.

```ruby
    class ActionNewOrder(Action):
    name = 'new_order'

    def __init__(self, order, exchange, original_event):
        super().__init__(exchange, original_event)
        self.order = order

```
```ruby
    class ActionCancel(Action):
    name = 'cancel'

    def __init__(self, symbol, price, order_id, exchange, original_event):
        super().__init__(exchange, original_event)
        self.symbol = symbol
        self.price = price
        self.order_id = order_id

```
```ruby
class ActionClosePosition(Action):
    name = 'close_position'

    def __init__(self, symbol, order_type, price, order_id, exchange, original_event):
        super().__init__(exchange, original_event)
        self.symbol = symbol
        self.price = price
        self.order_id = order_id
        self.order_type = order_type
```

<h3>Exchange Interfaces</h3>
در این بخش از کد برای هر یک از صرافی های مورد پشتیبانی کد یک رابط کاربری تعریف شده که با استفاده از ای پی ای صرافی توابع مورد نیاز آن پیاده سازی شده است.

```ruby
    def update_balance(self)
    def get_trading_symbols(self)
    def set_balance(self, balances)
    def on_balance_update(self, upd_balance_ev)
    def get_open_orders(self)
    def _cancel_order(self, order_id, symbol)
    def on_cancel_handler(self, event: Actions.ActionCancel)
    def _cancel_order_detector(self, price)
    def process_event(self, event)
    def on_order_handler(self, event: Actions.ActionNewOrder)
    def create_order(self, order)

```

<h3>Config files</h3>
در این دایرکتوری اطلاعات کاربران کپی کننده و کپی شونده و سایر 
اطلاعاتی مانند لیست ارزها ذخیره میشود

```ruby
    {
  "master": {
    "name": "Master" ,
    "key":"" ,
    "secret" :"",
    "exchange_name": "BitmexExchange"
     },
  "slaves":
    [{
      "name": "Slave1",
      "key": "",
      "secret": "",
      "exchange_name": "BitmexExchange"
      },
    {
      "name": "Slave2",
      "key": "",
      "secret": "",
      "exchange_name": "BinanceExchange"

    }
    ],
  "settings": {

  }
}
```
<h3>Slave Container</h3>
در این بخش از کد عملیات اصلی مربوط به کپی تریدینگ انجام میشود. 
ابتدا برای همه کاربران با استفاده از کانفیگ آنها و رمز و کلیدشان رابط کاربری با صرافی شان ساخته میشود.
اگر دنبال کنندگان صرافی متفاوتی از کپی شونده خود داشته باشند به طور اتومات از لیست دنبال کنندگان حذف میشوند.
سپس توسط تابع زیر تمام عملیاتی که برای کپی شونده انجام میشود به صورت همزمان برای دنبال کنندگان آن نیز انجام میشود.
در ساخت هر یک از رایط ها تابع زیر به سوکت پاس داده میشود که توسط این تابع تمامی عملیاتی که دنبال شونده انجام میدهد شناسایی شود.

```ruby
     def on_event_handler(self, event):
        # callback for event new order
        self.logger.debug(f'Event came: {event}')

        try:
            p_event = self.master.process_event(event)
        except:
            self.logger.exception('Error in master.process_event()')

        if p_event is None:
            # ignore this event
            return
        self.logger.info('\n')
        self.logger.info(f'New action came: {p_event}')

        if isinstance(p_event, Actions.ActionCancel):
            for slave in self.slaves:
                asyncio.run(slave.on_cancel_handler(p_event))
        elif isinstance(p_event, Actions.ActionNewOrder):
            for slave in self.slaves:
                asyncio.run(slave.on_order_handler(p_event))
        elif isinstance(p_event, Actions.ActionClosePosition):
            for slave in self.slaves:
                asyncio.run(slave.close_position(p_event))

        # store order_id of master order to relate it with slave order
        if isinstance(p_event, Actions.ActionNewOrder):
            for slave in self.slaves:
                slave.ids.append(p_event.order.id)

        # delete already not existed order ids to avoid memory leak
        elif isinstance(p_event, (Actions.ActionClosePosition, Actions.ActionCancel)):
            ord_id = p_event.order_id
            for slave in self.slaves:
                if slave.is_program_order(ord_id):
                    slave.delete_id(ord_id)


```


<h3>API</h3>
کد فلاسک برنامه که اکشن های وب اپلیکیشن را هندل میکند در این بخش قرار میگیرد.
اجرای برنامه و تابع اصلی نیز در همین بخش قرار میگیرد.

<h3>Statics and Templates</h3>
در این بخش از کد استایل ها و تمپلیت های مربوط به فرانت برنامه پیاده سازی شده است که برای اضافه کردن اطلاعات کپی شونده و کپی کننده های مربوط به آن استفاده میشود.
به این صورت که ابتدا کانفیگ مربوز که اکانت کپی شونده و سپس کانفیگ دنبال کننده های آن به صورت دستی به برنامه داده میشود.
